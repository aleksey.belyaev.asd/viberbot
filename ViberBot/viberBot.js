const ngrok = require('./getPublicUrl.js');
const ViberBot = require('viber-bot').Bot;
const BotEvents = require('viber-bot').Events;
const TextMessage = require('viber-bot').Message.Text;

require('dotenv').config();


// Creating the bot with access token, name and avatar
const bot = new ViberBot({
    authToken: process.env.AUTH_TOKEN,
    name: process.env.BOT_NAME,
    avatar: process.env.BOT_AVATAR
});

const http = require('http');
const port = process.env.PORT || 8080;

return ngrok.getPublicUrl().then(publicUrl => {
    console.log('Set the new webhook to"', publicUrl);
    http.createServer(bot.middleware()).listen(port, () => bot.setWebhook(publicUrl));
    
    bot.on(BotEvents.MESSAGE_RECEIVED, (message, response) => {
        
        if(message.text.includes("ping"))
            response.send(new TextMessage('pong'));
        else
            response.send(new TextMessage('Hi'));
    });
    
}).catch(error => {
    console.log('Can not connect to ngrok server. Is it running?');
    console.error(error);
});